const Feedback = {
    render() {
        return /* html */`
            <section class="py-16 mt-16 bg-center bg-cover bg-no-repeat group" style="background-image: url(https://res.cloudinary.com/dizzurnqo/image/upload/v1698158507/fback_ajqg7r.jpg)">
                <div class="container max-w-6xl mx-auto">
                    <div>
                        <h3 class="uppercase text-center block text-[#D9A953] text-2xl font-semibold">KHÁCH HÀNG NÓI GÌ</h3>
                        <p class="text-center text-sm text-gray-300 font-semibold mt-1">1500+ KHÁCH HÀNG HÀI LÒNG</p>
                    </div>

                    <ul id="home__feedback">
                        <li class="text-center mt-9">
                            <img src="https://res.cloudinary.com/dizzurnqo/image/upload/v1698158754/fback1_a1f5mi.jpg" alt="" class="w-24 h-24 object-cover rounded-full mx-auto">
                            <ul class="flex text-yellow-500 justify-center mt-2">
                            <li>
                                <i class="fas fa-star"></i>
                            </li>
                            <li>
                                <i class="fas fa-star"></i>
                            </li>
                            <li>
                                <i class="fas fa-star"></i>
                            </li>
                            <li>
                                <i class="fas fa-star"></i>
                            </li>
                            <li>
                                <i class="fas fa-star"></i>
                            </li>
                        </ul>
                        <p class="font-semibold text-gray-300 text-xl italic">Dương Ngọc Sơn</p>
                        <p class="mt-1 text-gray-300">
                            Mình rất thích đưa khách hàng của mình đến đây bởi vì
                            phong cách rất chuyên nghiệp.Hơn nữa thức uống ở đây rất ngon,
                            có hương vị rất khác biệt, các vị khách của mình vô cùng thích.
                        </p>
                        </li>
                        <li class="text-center mt-9">
                            <img src="https://res.cloudinary.com/dizzurnqo/image/upload/v1698159068/fback5_oucbw9.jpg" alt="" class="w-24 h-24 object-cover rounded-full mx-auto">
                            <ul class="flex text-yellow-500 justify-center mt-2">
                                <li>
                                    <i class="fas fa-star"></i>
                                </li>
                                <li>
                                    <i class="fas fa-star"></i>
                                </li>
                                <li>
                                    <i class="fas fa-star"></i>
                                </li>
                                <li>
                                    <i class="fas fa-star"></i>
                                </li>
                                <li>
                                    <i class="fas fa-star"></i>
                                </li>
                            </ul>
                            <p class="font-semibold text-gray-300 text-xl italic">Trần Thanh Hương</p>
                            <p class="mt-1 text-gray-300">
                            Nếu như muốn được thư giãn hãy nghe một bản nhạc.
                            Nếu muốn tìm một hương vị trà chanh đúng gu nhất với mình thì hãy đến với Tea House.
                            Nơi luôn khiến mình hài lòng nhất.
                            </p>
                        </li>
                        <li class="text-center mt-9">
                            <img src="https://res.cloudinary.com/dizzurnqo/image/upload/v1698159383/fback6_sw4puq.jpg" alt="" class="w-24 h-24 object-cover rounded-full mx-auto">
                            <ul class="flex text-yellow-500 justify-center mt-2">
                                <li>
                                    <i class="fas fa-star"></i>
                                </li>
                                <li>
                                    <i class="fas fa-star"></i>
                                </li>
                                <li>
                                    <i class="fas fa-star"></i>
                                </li>
                                <li>
                                    <i class="fas fa-star"></i>
                                </li>
                                <li>
                                    <i class="fas fa-star"></i>
                                </li>
                            </ul>
                            <p class="font-semibold text-gray-300 text-xl italic">Phạm Quang Linh</p>
                            <p class="mt-1 text-gray-300">
                            Không gian được thiết kế quá tuyệt vời luôn giúp mình có nhiều idea và cảm hứng để mình sáng tạo.
                            Hơn nữa chất lượng đồ uống ở đây vô cùng vừa ý mình, Tea House là sự lựa chọn tuyệt vời.
                            </p>
                        </li>
                    </ul>
                </div>
            </section>
        `;
    },
};
export default Feedback;