const Show = {
    render() {
        return /* html*/ `
        <section class="px-3 py-9">
            <div class="text-center">
                <h3 class="uppercase text-center block text-[#D9A953] text-2xl font-semibold">#trasuacocomoco</h3>
                <p>Những hình ảnh update từ Instagram.</p>
            </div>

            <div class="mt-5 group" id="home__show">
                <div class="px-1">
                <div style="background-image: url(https://res.cloudinary.com/dizzurnqo/image/upload/v1698593632/sp3_u7j3av.jpg);" class="bg-cover bg-center bg-no-repeat pt-[100%] rounded-xl"></div>
            </div>
            <div class="px-1">
                <div style="background-image: url(https://res.cloudinary.com/dizzurnqo/image/upload/v1698593630/sp1_v19g0w.jpg);" class="bg-cover bg-center bg-no-repeat pt-[100%] rounded-xl"></div>
            </div>
            <div class="px-1">
                <div style="background-image: url(https://res.cloudinary.com/dizzurnqo/image/upload/v1698593629/sp9_ktrahw.jpg);" class="bg-cover bg-center bg-no-repeat pt-[100%] rounded-xl"></div>
            </div>
            <div class="px-1">
                <div style="background-image: url(https://res.cloudinary.com/dizzurnqo/image/upload/v1698593629/sp10_cwtfln.jpg);" class="bg-cover bg-center bg-no-repeat pt-[100%] rounded-xl"></div>
            </div>
            <div class="px-1">
                <div style="background-image: url(https://res.cloudinary.com/dizzurnqo/image/upload/v1698593629/sp6_brnvaj.jpg);" class="bg-cover bg-center bg-no-repeat pt-[100%] rounded-xl"></div>
            </div>
            <div class="px-1">
                <div style="background-image: url(https://res.cloudinary.com/dizzurnqo/image/upload/v1698593629/sp4_gxbmxm.jpg);" class="bg-cover bg-center bg-no-repeat pt-[100%] rounded-xl"></div>
            </div>
            <div class="px-1">
                <div style="background-image: url(https://res.cloudinary.com/dizzurnqo/image/upload/v1698593628/sp7_vd9ggw.jpg);" class="bg-cover bg-center bg-no-repeat pt-[100%] rounded-xl"></div>
            </div>
            <div class="px-1">
                <div style="background-image: url(https://res.cloudinary.com/dizzurnqo/image/upload/v1698593632/sp5_zh5s7k.jpg);" class="bg-cover bg-center bg-no-repeat pt-[100%] rounded-xl"></div>
            </div>
            <div class="px-1">
                <div style="background-image: url(https://res.cloudinary.com/dizzurnqo/image/upload/v1698593629/sp8_pw0j1c.jpg);" class="bg-cover bg-center bg-no-repeat pt-[100%] rounded-xl"></div>
            </div>
            <div class="px-1">
                <div style="background-image: url(https://res.cloudinary.com/dizzurnqo/image/upload/v1698593630/sp2_vky8th.jpg);" class="bg-cover bg-center bg-no-repeat pt-[100%] rounded-xl"></div>
            </div>
            </section>
        `;
    },
};
export default Show;